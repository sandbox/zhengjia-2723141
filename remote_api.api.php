<?php
/**
 * @file
 * Sample hooks demonstrating usage in Remote APi.
 */

/**
 * List of remote api.
 */
function hook_remote_api() {

  $items = array();

  $items['example'] = array(
    'title'         => 'example',
    'description'   => 'description',
    'headers'       => array(),
    'url'           => 'http://www.xxx.com/',
    'method'        => 'GET',
    'args'          => array(),
    'args_callback' => 'function_name',
    'expire'      => 5 * 60,
  );

  return $items;
}
