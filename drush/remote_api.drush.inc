<?php

/**
 * @file
 * Drush integration for remote_api.
 */

/**
 * Implements hook_drush_command().
 */
function remote_api_drush_command() {

  $items = array();

  $items['cache_remote_api_data'] = array(
    'description' => dt("Remote api's response data will be stored."),
    'callback' => 'drush_cache_remote_api_response',
    'arguments' => array(
      'key' => dt('Optional. The cache ID of the data to store.'),
    ),
    'options' => array(
      'force' => 'All data was stored.',
      'select' => 'Choose which remote api to cache.',
    ),
    'aliases' => array('crad'),
    'examples' => array(
      'drush crad' => 'The expire entries will be updated.',
      'drush crad static_page' => 'The data of "static_page" will be stored.',
      'drush crad --force' => 'All data will be stored.',
      'drush crad --select' => 'Select which api will be stored.'
    ),
  );

  $items['cache_clear_remote_api_data'] = array(
    'description' => dt('Removes remote api cache data.'),
    'callback' => 'drush_cache_clear_remote_api_data',
    'arguments' => array(
      'key' => dt('Optional. Cache ID of remote api.'),
    ),
    'options' => array(
      'force' => 'Delete all data from the database.',
      'select' => 'Choose which remote api to clear.',
    ),
    'aliases' => array('ccrad'),
    'examples' => array(
      'drush ccrad' => 'The expire entries are deleted.',
      'drush ccrad static_page' => 'The cache entry of "static_page" will be deleted.',
      'drush ccrad --force' => 'All cache entries are deleted.',
      'drush ccrad --select' => 'Select which api will be deleted.'
    ),
  );

  $items['cache_remote_api_list'] = array(
    'description' => dt('Show the list of all remote api.'),
    'callback' => 'drush_cache_remote_api_list',
    'outputformat' => array(
      'default' => 'table',
      'pipe-format' => 'list',
      'field-labels' => array(
        'cid' => 'Cache ID',
        'title' => 'Title',
        'description' => 'Description',
      ),
      'output-data-type' => 'format-table',
    ),
    'aliases' => array('cral'),
  );

  return $items;
}

/**
 * A command callback.
 *
 * @param string $key
 *    The key of the data to store.
 */
function drush_cache_remote_api_response($key = NULL) {

  $result = module_invoke_all('remote_api');

  if ($key) {

    if ($result[$key]) {

      remote_api_set_cache($key, $result[$key]);

    }
    else {

      drush_log(dt('Remote api "@key" invalid.', array('@key' => $key)), 'error');

    }
  }
  else {

    $force = drush_get_option('force', FALSE);

    $select = drush_get_option('select', FALSE);

    if ($force) {

      remote_api_remove_invalid_entries($result);

      foreach ($result as $key => $row) {

        if ($row['token']) {

          continue;

        }

        remote_api_set_cache($key, $row);

      }

    }
    elseif ($select) {

      $keys = array();

      foreach ($result as $key => $row) {

        if ($row['token']) {

          continue;

        }

        $keys[$key] = array(
          $key,
          $row['title'],
          $row['description'],
        );

      }

      $key = drush_choice($keys, 'Enter a number to choose which remote api to cache.', '!key');

      if (empty($key)) {

        return drush_user_abort();

      }

      remote_api_set_cache($key, $result[$key]);
    }
    else {

      foreach (array_keys($result) as $key) {

        if ($result[$key]['token']) {

          continue;

        }

        $cache = cache_get($key, 'cache_remote_api');

        if ($cache->expire < time()) {

          remote_api_set_cache($key, $result[$key]);

        }

      }

    }
  }
}

/**
 * A command callback.
 *
 * @param string $key
 *    The key of the data to retrieve.
 */
function drush_cache_clear_remote_api_data($key = '') {

  if ($key) {

    cache_clear_all($key, 'cache_remote_api');

    drush_log(dt('"@key" cache was cleared.', array('@key' => $key)), 'success');

  }
  else {

    $force = drush_get_option('force', FALSE);

    $select = drush_get_option('select', FALSE);

    if ($force) {

      cache_clear_all('*', 'cache_remote_api', TRUE);

      drush_log(dt('"all" cache was cleared.'), 'success');

    }
    elseif ($select) {

      $result = remote_api_get_list();

      $keys = array();

      foreach ($result as $key => $row) {

        $keys[$key] = array(
          $key,
          $row['title'],
          $row['description'],
        );

      }

      $key = drush_choice($keys, 'Enter a number to choose which cache to clear.', '!key');

      if (empty($key)) {

        return drush_user_abort();

      }

      cache_clear_all($key, 'cache_remote_api', TRUE);

      drush_log(dt('"@key" cache was cleared.', array('@key' => $key)), 'success');
    }
    else {

      cache_clear_all(NULL, 'cache_remote_api');

      drush_log(dt('The expire entries was cleared.'), 'success');

    }

  }
}

/**
 * A command callback.
 *
 * @return array
 *    Return an array.
 */
function drush_cache_remote_api_list() {

  $result = remote_api_get_list();

  $list = array();

  foreach ($result as $key => $row) {

    if ($row['token']) {
      continue;
    }

    $list[] = array(
      'cid' => $key,
      'title' => $row['title'],
      'description' => $row['description'],
    );

  }

  return $list;
}
