# Remote API

Remote API module allows Drupal developers  to encapsulate remote apis from drupal multi-sites or third party apis. So develoepr can call thoses API in same way and in different mode.

## Installation

1. Extract module files into `sites/all/modules/` as normal module installation flow.

2. Enable the module as usual.

## Backend

1. The backend interface is on `admin/config/system/remote-api`；

2. Developers can see all implementations of hook_remote_api, and can overwrite hook info using backend settings.

## Three Modes

+ **REMOTE_API_CACHE_INITIATIVE** Default mode, get data from cache, if not exist, build one using api hook info.
+ **REMOTE_API_CACHE_NORMAL** Get data from cache, if not exist return null.
+ **REMOTE_API_CACHE_DISABLE** Disable cache, get data from remote api directly.

## Drush Commands

+ `drush crad` Build cache
+ `drush crad --select`
+ `drush crad --force`
+ `drush ccrad` Clear cache
+ `drush ccrad --select`
+ `drush ccrad --force`
+ `drush cral` List hook implementations

## Usage

```php
function example_remote_api(){
  $items = array();

  $items['REMOTE_API_KEY'] = array(
    'title'         => 'TITLE',
    'description'   => 'DESCRIPTION',
    'url'           => 'http://www.example.com/api/info/:id',
    'headers'       => array('Content-Type' => 'application/x-www-form-urlencoded'),
    'method'        => 'POST',
    'token'         => TRUE,
    'mode'          => REMOTE_API_CACHE_INITIATIVE
  );

  return $items;
}
```

** Need clear cache after implementing a hook.

The api function to get data from any remote api：

```php
$data = remote_api_get_cache('REMOTE_API_KEY',
  array(
    'args' => array(
      'PARAMS_KEY' => PARAMS_VALUE,
    ),
    'token' => array(
      ':id' => ID
    ),
  )
);
```
